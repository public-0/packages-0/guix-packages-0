;;; GNU Guix --- Functional package management for GNU
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (my-packages packages my-emacs-xyz)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix cvs-download)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix hg-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system emacs)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages code)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages dictionaries)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages telephony)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages texinfo)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages lesstif)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages music)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages imagemagick)
  #:use-module (gnu packages w3m)
  #:use-module (gnu packages wget)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages node)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages acl)
  #:use-module (gnu packages mail)
  #:use-module (gnu packages messaging)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu packages scheme)
  #:use-module (gnu packages speech)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages mp3)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages gd)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages video)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages wordnet)
  #:use-module (gnu packages photo)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match))

(define-public my-emacs-ivy
  (let ((commit "c7973a8c3bdac71e2e7e5fd2112e5408d1f3de4d")
        (revision "1"))
    (package (inherit emacs-ivy)
             (name "my-emacs-ivy")
             (version (git-version "0.13.0" revision commit))
             (source
              (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/abo-abo/swiper.git")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1g604779d9bm9bfqql6vc0xdrc8f5av55ya8qa3p0z4agq5m3as3")))))))


(define-public my-emacs-lsp-mode
  (let ((commit "355d4da3ac16d801ce4760f70075997928418186")
        (revision "1"))
    (package (inherit emacs-lsp-mode)
             (name "my-emacs-lsp-mode")
             (version (git-version "6.2.1" revision commit))
             (source (origin
                       (method git-fetch)
                       (uri (git-reference
                             (url "https://github.com/emacs-lsp/lsp-mode.git")
                             (commit commit)))
                       (file-name (git-file-name name version))
                       (sha256
                        (base32
                         "1zpp54bm666ia82ww4zwqzk6cx0a6f0fjnzp4ggkiiwqlvx5r57s")))))))

(define-public my-emacs-lsp-ui
  (let ((commit "70c2fecbabe663a6af4e9414cafc7143f30b2f93")
        (revision "1"))
    (package (inherit emacs-lsp-ui)
             (name "my-emacs-lsp-ui")
             (version (git-version "6.2" revision commit))
             (source (origin
                       (method git-fetch)
                       (uri (git-reference
                             (url "https://github.com/emacs-lsp/lsp-ui.git")
                             (commit commit)))
                       (file-name (git-file-name name version))
                       (sha256
                        (base32
                         "104nwb3k9dxjxc7cz6sxncx9fpcwai8kp22d4d3fr08rm612696m")))))))

(define-public my-emacs-transient
  (package (inherit emacs-transient)
           (name "my-emacs-transient")
           (version "0.2.0")
           (source (origin
                     (method git-fetch)
                     (uri (git-reference
                           (url "https://github.com/magit/transient")
                           (commit (string-append "v" version))))
                     (file-name (git-file-name name version))
                     (sha256
                      (base32
                       "0w50sh55c04gacx2pp19rvi0fwj9h19c9gzd8dpa82zjiidfxckr"))))
           (license license:gpl3+)))

;; (define-public my-emacs-magit
;;   (let ((commit "c8cd22e28d20b0d7ae74102df4e35d7c0c61ebe6")
;;         (version "2.90.1")
;;         (revision "4"))
;;     (package (inherit emacs-magit)
;;              (name "my-emacs-magit")
;;              (version (git-version version revision commit))
;;              (source (origin
;;                        (method git-fetch)
;;                        (uri (git-reference
;;                              (url "https://github.com/magit/magit.git")
;;                              (commit commit)))
;;                        (file-name (git-file-name name version))
;;                        (sha256
;;                         (base32
;;                          "134n9m2zrf60ljm867bv92qjgk61cmgns1a3cqpvj4wflpaahnbv"))
;;                        (modules '((guix build utils)))
;;                        (snippet
;;                         '(begin
;;                            ;; Fix syntax error
;;                            (substitute* "lisp/magit-extras.el"
;;                              (("rev\\)\\)\\)\\)\\)\\)") "rev)))))"))
;;                            #t))))
;;              (propagated-inputs
;;               `(("dash" ,emacs-dash)
;;                 ("with-editor" ,emacs-with-editor)
;;                 ("my-transient" ,my-emacs-transient)))
;;              (arguments
;;               `(#:modules ((guix build gnu-build-system)
;;                            (guix build utils)
;;                            (guix build emacs-utils))
;;                 #:imported-modules (,@%gnu-build-system-modules
;;                                     (guix build emacs-utils))
;;                 #:test-target "test"
;;                 #:tests? #f                   ; tests are not included in the release
;;                 #:make-flags
;;                 (list (string-append "PREFIX=" %output)
;;                       ;; Don't put .el files in a sub-directory.
;;                       (string-append "lispdir=" %output "/share/emacs/site-lisp")
;;                       ;; Don't build with libgit
;;                       "BUILD_MAGIT_LIBGIT=false"
;;                       )
;;                 #:phases
;;                 (modify-phases %standard-phases
;;                   (add-after 'unpack 'patch
;;                     (lambda _
;;                       (chmod "lisp/magit-extras.el" #o644)
;;                       (emacs-batch-edit-file "lisp/magit-extras.el"
;;                         `(progn (progn
;;                                  (goto-char (point-min))
;;                                  (re-search-forward "(defun magit-copy-buffer-revision ()")
;;                                  (forward-sexp 2)
;;                                  (kill-sexp)
;;                                  (insert ,(format #f "~S"
;;                                                   '(if (use-region-p)
;;                                                        (copy-region-as-kill nil nil 'region)
;;                                                        (when-let ((rev (cl-case major-mode
;;                                                                                 ((magit-cherry-mode
;;                                                                                   magit-log-select-mode
;;                                                                                   magit-reflog-mode
;;                                                                                   magit-refs-mode
;;                                                                                   magit-revision-mode
;;                                                                                   magit-stash-mode
;;                                                                                   magit-stashes-mode)
;;                                                                                  (car magit-refresh-args))
;;                                                                                 ((magit-diff-mode magit-log-mode)
;;                                                                                  (let ((r (caar magit-refresh-args)))
;;                                                                                    (if (string-match "\\.\\.\\.?\\(.+\\)" r)
;;                                                                                        (match-string 1 r)
;;                                                                                        r)))
;;                                                                                 (magit-status-mode "HEAD"))))
;;                                                                  (when (magit-commit-p rev)
;;                                                                    (setq rev (magit-rev-parse rev))
;;                                                                    (push (list rev default-directory) magit-revision-stack)
;;                                                                    (kill-new (message "%s" rev))))))))
;;                                 (basic-save-buffer)))
;;                       #t))
;;                   (delete 'configure)
;;                   (add-before
;;                       'build 'patch-exec-paths
;;                     (lambda* (#:key inputs #:allow-other-keys)
;;                       (let ((perl (assoc-ref inputs "perl")))
;;                         (make-file-writable "lisp/magit-sequence.el")
;;                         (emacs-substitute-variables "lisp/magit-sequence.el"
;;                           ("magit-perl-executable" (string-append perl "/bin/perl")))
;;                         #t)
;;                       (let ((output-port (open-file "lisp/magit-version.el" "w"))
;;                             (commit "c8cd22e28d20b0d7ae74102df4e35d7c0c61ebe6")
;;                             (version "2.90.1")
;;                             )
;;                         (display ";; magit-version.el --- -*- lexical-binding: t; -*-" output-port)
;;                         (newline output-port)
;;                         (display (string-append "(setq magit-version \"" version " (" commit ")\")") output-port)
;;                         (newline output-port)
;;                         (display "(provide 'magit-version)

;; ;; Local Variables:
;; ;; no-byte-compile: t
;; ;; no-update-autoloads: t
;; ;; coding: utf-8
;; ;; End:

;; ;; magit-version.el ends here" output-port)
;;                         (newline output-port)
;;                         (close output-port))))))))))
