(define-module (my-packages packages editline)
  #:use-module (guix licenses)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu))

(define-public editline
  (package
   (name "editline")
   (version "1.17.1")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/troglobit/editline.git")
                  (commit version)))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "0y7vis52kfq2km9isrlzakpf7bn1k7v9vf3xg3q1cslla18q6myh"))))
   (build-system gnu-build-system)
   (outputs '("out"))
   (inputs
    `(
      ("autoconf" ,autoconf)
      ("automake" ,automake)
      ("libtool" ,libtool)
      ("pkg-config" ,pkg-config)
      ))
   (arguments
    `(#:configure-flags (list (string-append "-prefix=" (assoc-ref %outputs "out")) )))
   (home-page "https://github.com/troglobit/editline")
   (synopsis "Small Editline library for UNIX")
   (description
    "This is a small line editing library. It can be linked into almost any
program to provide command line editing and history functions. It is call
compatible with the FSF readline library, but at a fraction of the size, and as
a result fewer features.")
   (license bsd-3)))
