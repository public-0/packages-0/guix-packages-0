;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (my-packages packages my-fonts)
  #:use-module (ice-9 regex)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system font)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages xorg))

(define-public my-font-xits
  (package
    (name "my-font-xits")
    (version "1.301")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/alif-types/xits/releases/download/"
                    "v" version "/XITS-" version ".zip"))
              (sha256
               (base32
                "0wymiyc7r1n7fxn1h2wlggv07kmf8icp3zk8r8dy9y8hqd2bkbcq"))))
    (build-system font-build-system)
    (home-page "https://github.com/alif-types/xits")
    (synopsis "XITS typeface")
    (description "XITS is a Times-like typeface for mathematical and
scientific publishing, based on STIX fonts. The main mission of XITS is to
provide a version of STIX fonts enhanced with the OpenType MATH table, making
it suitable for high quality mathematic typesetting with OpenType math-capable
layout systems, like Microsoft Office 2007+, XeTeX and LuaTeX.")
    (license license:silofl1.1)))
